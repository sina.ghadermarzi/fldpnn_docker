# flDPnn on Docker

This is the docker implementation of the [flDPnn](http://biomine.cs.vcu.edu/servers/flDPnn/) predictor, and simplifies the use of this tool for prediction tasks. The source codes are provided in [another repository](https://gitlab.com/sina.ghadermarzi/fldpnn).
The instructions and dockerfile are tested on Ubuntu 20.04.2 LTS (and docker version detailed in [docker_version.txt](https://gitlab.com/sina.ghadermarzi/fldpnn_docker/-/blob/master/docker_version.txt)).

## Instructions

To use flDPnn on docker, you need to have docker engine on your machine. Instruction for installation of docker engine can be found on [docker website](https://docs.docker.com/engine/install/).

also run the following commond to make sure your docker is installed correctly:
`docker run hello-world`

if you get permission error, you may need to do the following configuration (copied from [here](https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue)) 
1. Create the docker group if it does not exist

    `sudo groupadd docker`

2. Add your user to the docker group.

    `sudo usermod -aG docker $USER`

3. Run the following command or Logout and login again and run (that doesn't work you may need to reboot your machine first)

    `newgrp docker`


Once you have docker engine ready, you can install and use the fldpnn predictor using one of the following ways:
### First way (easier): pulling the image from dockerhub
For this, follow these steps:
1. pull the image from dockerhub using the following command:

    `docker pull docker.io/sinaghadermarzi/fldpnn`


2. Run the docker image using the following command (including `<` and `>` characters).

    `docker run -i sinaghadermarzi/fldpnn fldpnn/dockerinout < test.fasta > fldpnn_results.tar.gz`

    Where:

    `test.fasta` is the path to your input fasta file.
    
    `fldpnn_results.tar.gz` is the path in which the output is saved as a tar archive
    
    **To run flDPnn for predicting disorder only (no function prediction and visualization) use the following command instead (This will be significantly faster):**
    
    `docker run -i sinaghadermarzi/fldpnn fldpnn/dockerinout_nofunc < test.fasta > fldpnn_results.csv`

### Second way: building image from dockerfile
1. Build the docker image using the provided docker file: `fldpnn.dockerfile`

    You can do this by entering the following command in the directory where the dockerfile is located:

    `docker build -t fldpnn - < fldpnn.dockerfile`


2. Run the docker image using the following command (including `<` and `>` characters).

    `docker run -i fldpnn fldpnn/dockerinout < test.fasta > fldpnn_results.tar.gz`

    Where:

    `test.fasta` is the path to your input fasta file.
    
    `fldpnn_results.tar.gz` is the path in which the output is saved as a tar archive
    
    **To run flDPnn for predicting disorder only (no function prediction and visualization) use the following command instead (This will be significantly faster):**
    
    `docker run -i fldpnn fldpnn/dockerinout_nofunc < test.fasta > fldpnn_results.csv`
## Updates
- **2021 December** - Updated residue numbers in the output visualization. Updated output csv format.
- **2021 March** - Initial release

